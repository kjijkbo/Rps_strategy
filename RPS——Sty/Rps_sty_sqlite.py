import tushare as ts
import pandas as pd
import MySQLdb as mysql
import sqlite3
import talib as ta
import json
import csv
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import numpy as np
import traceback
from sqlalchemy.sql.expression import false




#db=mysql.connect('localhost','root','Aa888888','rpsdata2')
#cursor=db.cursor()
conn=sqlite3.connect('c:\\sqlite\\securityevaluationdb.db')

cursor_stk=conn.cursor()
cursor_stk.row_factory = sqlite3.Row
#db_stk=mysql.connect('localhost','root','Aa888888','stockinfo')
#cursor_stk=db_stk.cursor()

#file1=open('D:\\GEM_SME.txt')
#symlist=file1.readlines()
#file1.close()
total=0
fileout=open('D:\\RPSbuy&hold.txt','w')
filetralist=open('D:\\netdetail.txt','w')
tot_profit=0


def datelist(start,end):
    if start[5:7]<'06':
        temp=start[:4]+'-06-01' 
    else:
        temp=str(int(start[:4]) + 1) + '-06-01'
    if start[:4]==end[:4] or end<temp:
        sh = ts.get_k_data('399300', index=True, start=start, end=end)
        sh.sort(['date'], ascending=True, inplace=True)
        sh.index=sh['date']
        return sh.index.values, sh
    sh=ts.get_k_data('399300',index=True,start=start,end=temp)
    starttemp=temp
    temp=str(int(starttemp[:4])+1)+'-06-01'
    while end>temp:
        sh1=ts.get_k_data('399300',index=True,start=starttemp,end=temp)
        sh=pd.merge(sh,sh1,how='outer')
        starttemp = temp
        temp = str(int(starttemp[:4]) + 1) + '-06-01'
    sh1 = ts.get_k_data('399300', index=True, start=starttemp, end=end)
    sh = pd.merge(sh, sh1, how='outer')
    sh.sort(['date'], ascending=True, inplace=True)
    sh.index=sh['date']
    return sh.index.values,sh

winpoint=1.05
atrcheck=True
tdate,shdata = datelist('2010-01-01', '2016-12-01')
Sdate='2014-04-21'
Edate='2016-05-31'
shdata['MA60'] = ta.MA(shdata['close'].values.astype('float'), timeperiod=20)
def genacct(start,end):
    date=datelist(start,end)
    acct={}
    for i in range(0,len(date)-1):
        acct.setdefault('date',[]).append(date[i])
        acct.setdefault('pos',[]).append(0)
    df=pd.DataFrame(acct)
    df.index=df['date']
    df = df.sort_index()
    return df
def loaddata(sym):
    sql = 'select dateTime,openP,maxP,minP,closeP,money,total from StockHisKLineFCR where klineType=1 and isValid=1 and isFillData=0 and stockID=(select stockID from StockStk where stockCode=\''
    sql=sql+sym[:6]+'\' and stockMarket =\''+sym[7:]+'\' limit 1) and date(dateTime)>\'2015-01-01\''
    results=conn.execute(sql)
    data = {}
    try:
        for row in results:
            data.setdefault('date', []).append (str(row[0])[:4]+str(row[0])[5:7]+str(row[0])[8:10])
            data.setdefault('open', []).append (float(row[1]))
            data.setdefault('high', []).append (float(row[2]))
            data.setdefault('low', []).append( float(row[3]))
            data.setdefault('close', []).append(float(row[4]))
            data.setdefault('money', []).append(int(row[5]))
            data.setdefault('volume', []).append(row[6])
    except:
        print
    df=pd.DataFrame(data)
    df.index=df['date']
    df = df.sort_index()
    return df

def loadpoint(sym,date,field):
    try:
        sql = 'select '+field+' from StockHisKLineFCR where klineType=1 and isValid=1 and isFillData=0 and stockID=(select stockID from StockStk where stockCode=\''
        sql=sql+sym[:6]+'\' and stockMarket =\''+sym[7:]+'\' limit 1) and date(dateTime)=\''+date+'\''
        results=conn.execute(sql)
        data = {}
        for row in results:
            data.setdefault('value', []).append(row[0])
    except:
        print 'error in loadpoint'
        #sql = 'select ' + field + ' from StockHisKLineFCR where klineType=1 and isValid=1 and isFillData=0 and stockID=(select stockID from StockStk where stockCode=\''
        #sql = sql + sym[:6] + '\' and stockMarket =\'' + sym[7:] + '\' limit 1) and date(dateTime)<=\'' + date + '\' order by datetime desc limit 1'
    df=pd.DataFrame(data)
    return df['value'].values[0]

def loaddatablocks(sym,date):
    try:
        datestart = tdate[np.where(tdate == date)[0] - 9][0]
        sql = 'select openP,maxP,minP,closeP from StockHisKLineFCR where klineType=1 and isValid=1 and isFillData=0 and stockID=(select stockID from StockStk where stockCode=\''
        sql=sql+sym[:6]+'\' and stockMarket =\''+sym[7:]+'\' limit 1) and date(dateTime) between \''+datestart+'\' and \'' + date + '\''
        results=conn.execute(sql)
        data = {}
        for row in results:
            data.setdefault('open', []).append(float(row[0]))
            data.setdefault('high', []).append(float(row[1]))
            data.setdefault('low', []).append(float(row[2]))
            data.setdefault('close', []).append(float(row[3]))
    except:
        print 'error in loaddatablocks'
    df=pd.DataFrame(data)
    return df

def loadnearly(sym,date,field):
    try:
        sql = 'select ' + field + ' from StockHisKLineFCR where klineType=1 and isValid=1 and isFillData=0 and stockID=(select stockID from StockStk where stockCode=\''
        sql = sql + sym[:6] + '\' and stockMarket =\'' + sym[7:] + '\' limit 1) and date(dateTime)<=\'' + date + '\' order by datetime desc limit 1'
        results=conn.execute(sql)
        data = {}
        for row in results:
            data.setdefault('value', []).append(row[0])
    except:
        print 'error in loadnearly'
    # sql = 'select ' + field + ' from StockHisKLineFCR where klineType=1 and isValid=1 and isFillData=0 and stockID=(select stockID from StockStk where stockCode=\''
    # sql = sql + sym[:6] + '\' and stockMarket =\'' + sym[7:] + '\' limit 1) and date(dateTime)<=\'' + date + '\' order by datetime desc limit 1'
    df = pd.DataFrame(data)
    return df['value'].values[0]
def loadrpspoint(sym,date,field):
    try:
        sql = 'select '+field+' from StockRpsHist where stockID=(select stockID from StockStk where stockCode=\''
        sql=sql+sym[:6]+'\' and stockMarket =\''+sym[7:]+'\' limit 1) and date(dateTime)=\''+date+'\''
        results=conn.execute(sql)
        data = {}
        for row in results:
            data.setdefault('value', []).append(row[0])
    except:
        print 'error in loadrpspoint'
    df=pd.DataFrame(data)
    return df['value'].values[0]

def loadrps(sym):
    sql = 'select qsqr from a' + sym + ' where exchangecode>=20150101000000'
    results=conn.execute(sql)
    data={}
    try:
        for row in results:
            js = json.loads(row[0])
            data.setdefault('date', []).append(js['dt'][:8])
            data.setdefault('t1', []).append ( js['vp'][0])
            data.setdefault('t2', []).append ( js['vp'][1])
            data.setdefault('t3', []).append ( js['vp'][2])
            data.setdefault('t4', []).append ( js['vp'][3])
            data.setdefault('t5', []).append ( js['vp'][4])
            data.setdefault('t6', []).append ( js['vp'][5])
    except:
        print
    df = pd.DataFrame(data)
    df.index = df['date']
    df = df.sort_index()
    return df
def loadrpsbydate(date):
    sql='select stockstk.stockcode,stockstk.stockmarket,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d from stockrpshist,stockstk ' \
        'where stockstk.stockid=stockrpshist.stockid and date(dateTime)=\''+date+'\''
    #sql='select * from stockrpshist where dateTime=\''+date+'\''
    results=conn.execute(sql)
    data = {}
    try:
        for row in results:
            data.setdefault('symbol', []).append(row[0]+'.'+row[1])
            data.setdefault('rps1d', []).append(row[2])
            data.setdefault('rps3d', []).append(row[3])
            data.setdefault('rps5d', []).append(row[4])
            data.setdefault('rps10d', []).append(row[5])
            data.setdefault('rps20d', []).append(row[6])
            data.setdefault('rps30d', []).append(row[7])
            data.setdefault('rps65d', []).append(row[8])
            data.setdefault('rps130d', []).append(row[9])
            data.setdefault('rps260d', []).append(row[10])
    except:
        print
    df=pd.DataFrame(data)
    df.index=df.symbol
    return df
def count(stkdic,date):
    sum=0
    for stk in stkdic:
        price = loadnearly(stk, date, 'closeP')
        sum = sum + float(price) * stkdic[stk]
    return sum
def load_allrps(startdate):
    sql='select stockstk.stockcode,stockstk.stockmarket,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d,dateTime from stockrpshist,stockstk ' \
        'where stockstk.stockid=stockrpshist.stockid and dateTime> \''+startdate+'\''
    results=conn.execute(sql)
    data = {}
    try:
        for row in results:
            if not data.has_key(row[0]+'.'+row[1]):
                data[row[0]+'.'+row[1]].setdefault(row[11][:10],{})
            data[row[0]+'.'+row[1]][row[11][:10]]['rps1d']=row[2]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps3d']=row[3]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps5d']=row[4]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps10d']=row[5]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps20d']=row[6]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps30d']=row[7]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps65d']=row[8]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps130d']=row[9]
            data[row[0]+'.'+row[1]][row[11][:10]]['rps260d']=row[10]
    except:
        pass
    return data
def loadrpsblocks(date):
    datestart = tdate[np.where(tdate==date)[0]-2][0]
    timepot=datetime.datetime.now()
    sql='select stockstk.stockcode,stockstk.stockmarket,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d from stockrpsavg,stockstk ' \
        'where stockstk.stockid=stockrpsavg.stockid and rps65d>85 and rps130d>85 and date(dateTime)=\''+date+'\''
    #sql='select stockstk.stockcode,stockstk.stockmarket,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d from stockrpshist,stockstk ' \
     #   'where stockstk.stockid=stockrpshist.stockid and date(dateTime) between \''+datestart+'\' and \'' + date + '\''
    #sql='select stockstk.stockcode,stockstk.stockmarket,avg(rps1d) as rps1d,avg(rps3d) as rps3d,avg(rps5d) as rps5d,avg(rps10d) as rps10d,avg(rps20d) as rps20d,avg(rps30d) as rps30d,avg(rps65d) as rps65d,avg(rps130d) as rps130d,avg(rps260d) as rps260d from stockrpshist,stockstk where stockstk.stockid=stockrpshist.stockid and date(dateTime) between \''+datestart+'\' and \'' + date + '\' group by stockstk.stockCode'
    results=cursor_stk.execute(sql)
    conn.commit()
    results=results.fetchall()
    gap=datetime.datetime.now()-timepot
    print gap
    #df=pd.read_sql(sql, conn, index_col=None, coerce_float=True, params=None, parse_dates=None, columns=None,chunksize=None)
    #df.index=df.stockCode
    #gap=datetime.datetime.now()-timepot
   # print gap
    #return df
    
    
    data = {}
    data.setdefault('symbol',[])
    
    gap=datetime.datetime.now()-timepot
    print gap
    try:
        for row in results:
            if row[0]+'.'+row[1] in data['symbol']:
                index=data['symbol'].index(row[0]+'.'+row[1])
                data['rps1d'][index]=data['rps1d'][index]+row[2]
                data['rps3d'][index] = data['rps3d'][index] + row[3]
                data['rps5d'][index] = data['rps5d'][index] + row[4]
                data['rps10d'][index] = data['rps10d'][index] + row[5]
                data['rps20d'][index] = data['rps20d'][index] + row[6]
                data['rps30d'][index] = data['rps30d'][index] + row[7]
                data['rps65d'][index] = data['rps65d'][index] + row[8]
                data['rps130d'][index] = data['rps130d'][index] + row[9]
                data['rps260d'][index] = data['rps260d'][index] + row[10]
            else:
                data.setdefault('symbol', []).append(row[0]+'.'+row[1])
                data.setdefault('rps1d', []).append(row[2])
                data.setdefault('rps3d', []).append(row[3])
                data.setdefault('rps5d', []).append(row[4])
                data.setdefault('rps10d', []).append(row[5])
                data.setdefault('rps20d', []).append(row[6])
                data.setdefault('rps30d', []).append(row[7])
                data.setdefault('rps65d', []).append(row[8])
                data.setdefault('rps130d', []).append(row[9])
                data.setdefault('rps260d', []).append(row[10])
    except:
        print
    '''
    for i in range(0,len(data['symbol'])):
        data['rps1d'][i]=data['rps1d'][i]/3
        data['rps3d'][i] = data['rps3d'][i] / 3
        data['rps5d'][i] = data['rps5d'][i] / 3
        data['rps10d'][i] = data['rps10d'][i] / 3
        data['rps20d'][i] = data['rps20d'][i] / 3
        data['rps30d'][i] = data['rps30d'][i] / 3
        data['rps65d'][i] = data['rps65d'][i] / 3
        data['rps130d'][i] = data['rps130d'][i] / 3
        data['rps260d'][i] /= 3
    '''
    gap=datetime.datetime.now()-timepot
    print gap
    df=pd.DataFrame(data)
    df.index=df.symbol
    #gap=datetime.datetime.now()-timepot
    #print gap
    return df
def drawline(start,end,acct):
    try:
        position={}
        hsstartnet=float(shdata.close[np.where(tdate==start)[0]])
        startbalance=acct[start]
        for i in range(np.where(tdate==start)[0],np.where(tdate==end)[0]+1):  
            position.setdefault('date', []).append(tdate[i])
            position.setdefault('net',[]).append(acct[tdate[i]]/startbalance)
            position.setdefault('hs300', []).append(float(shdata['close'][tdate[i]]) /hsstartnet )
        #    position.setdefault('gap',[]).append(acct[tdate[i]]/100000000-float(shdata['close'][tdate[i]]) / float(shdata.close.values[60]))
        
        df = pd.DataFrame(position)
        df.index = df['date']
        df = df.sort_index()
        df.plot()
        plt.show()
    except:
        print 'draw error'

def stopwin(stk,date,entrypri):
    price = loadpoint(stk, date, 'openP')
    if price >= entrypri * winpoint:
        return True
    return False

def stoplose(stk,date,entrypri,deadline):
    price = loadpoint(stk, date, 'closeP')
    if price < entrypri * deadline:
        return True
    return False

def strongtest(tothold):
    acct={}
    cash={}
    BSE=10
    acctbalance=100000000.0
    stkpos={}
    entrypri={}
    expired={}
    maxpri={}
    stkbse={}
    stopstk={}
    change=False
    Clean=False
    for i in range(np.where(tdate==Sdate)[0],np.where(tdate==Edate)[0]+1):
        try:
            '''
            if BSE>=10:
                fileout.write('held expired\n')
                keys=list(stkpos.keys())
                for stk in keys:
                    try:
                        price=loadpoint(stk,tdate[i],'openP')
                        if float(loadpoint(stk,tdate[i],'maxP'))<=round(float(loadpoint(stk,tdate[i-1],'closeP'))*0.99,2):      #
                            raise Exception("no enough volumn")
                        acctbalance=acctbalance+float(price)*stkpos[stk]
                        change = True
                        del stkpos[stk]
                        del entrypri[stk]
                        del maxpri[stk]
                    except:
                        expired[stk]=stkpos[stk]
                        print 'can not sell ',stk
                BSE = 0
                Clean=True
                if len(stopstk)>0:
                    stopstk.clear()
            '''
            keys = list(stkpos.keys())
            
            for stk in keys:
                try:
                    stkbse[stk]+=1
                    try:
                        price = loadpoint(stk, tdate[i], 'openP')
                    except:
                        continue
                    try:
                        high = loadpoint(stk,tdate[i-1],'maxP')
                        maxpri[stk]=max(high,maxpri[stk])
                    except:
                        pass
                    #if (float(maxpri[stk])>1.1*float(entrypri[stk]) and stoplose(stk,tdate[i-1],float(maxpri[stk]),0.97)) or stoplose(stk,tdate[i-1],float(entrypri[stk]),0.95) or expired.has_key(stk):#or shdata.close[tdate[i-1]]<shdata.MA60[tdate[i-1]]:
                    #expired.has_key(stk) 
                    if stoplose(stk,tdate[i-1], float(entrypri[stk]),0.95) or stoplose(stk,tdate[i-1], float(maxpri[stk]),0.92)  or stkbse[stk]>20:
                        if loadpoint(stk, tdate[i], 'total') < 0.1*stkpos[stk] or float(loadpoint(stk,tdate[i],'maxP'))<=round(float(loadpoint(stk,tdate[i-1],'closeP'))*0.99,2): #
                            raise Exception("no enough volumn")
                        acctbalance = acctbalance + float(price) * stkpos[stk]
                        change = True
                        stopstk[stk]=1
                        del stkpos[stk]
                        del entrypri[stk]
                        del maxpri[stk]
                        del stkbse[stk]
                        if expired.has_key(stk):
                            del expired[stk]
                except:
                    pass
            
            try:
                if len(stkpos)<tothold:#len(stkpos)<tothold :#and shdata.close[tdate[i-1]]>shdata.MA60[tdate[i-1]]:
                    timepot=datetime.datetime.now()
                    #print datetime.datetime.now().second, datetime.datetime.now().microsecond
                    rpsdata = loadrpsblocks(tdate[i-1])#loadrpsbydate(tdate[i-1])
                    rpsdata.sort(['rps65d'], ascending=False, inplace=True)
                    percash = acctbalance / (tothold-len(stkpos))
                    Clean=False
                    j = len(rpsdata)-1
                    gap=datetime.datetime.now()-timepot
                    #print gap
                    while len(stkpos)<tothold and j>50:#and shdata.close[tdate[i-1]]>shdata.MA60[tdate[i-1]] and j<500:
                        try:
                            stk=rpsdata.symbol.values[j]
                            #stk=rpsdata.stockCode.values[j]+'.'+rpsdata.stockMarket.values[j]
                            #or stopstk.has_key(stk)
                            if stkpos.has_key(stk)  or rpsdata.rps65d[j]<85 or rpsdata.rps130d[j]<85 or rpsdata.rps1d[j]<rpsdata.rps3d[j] or rpsdata.rps1d[j]>50:#or rpsblock.rps1d[stk]>rpsdata.rps1d.values[j]:
                                j=j-1
                                continue
                            '''
                            if stkpos.has_key(stk):
                                j=j-1
                                continue 
                            if rpsdata.rps65d[j]<85 or rpsdata.rps130d[j]<85 or rpsdata.rps1d[j]<rpsdata.rps3d[j] or rpsdata.rps1d[j]>50:
                                j=j-1
                                continue
                            '''
                            #if loadrpspoint(stk, tdate[i-1], 'rps30d')>80:
                            #    j=j-1
                            #    continue
                            price = float(loadpoint(stk, tdate[i], 'openP'))
                            if price<0:
                                j=j-1
                                continue
                            qty = round(percash / price / 100) * 100
                            if float(loadpoint(stk, tdate[i-1], 'closeP'))*1.095<price:  #
                                raise Exception("can not buy at open")
                            data=loaddatablocks(stk,tdate[i-1])
                            data['atr'] = ta.ATR(data['high'].values, data['low'].values, data['close'].values,timeperiod=9)
                            if data.atr[9]>0.8 and atrcheck:  # or rpsblock.rps1d[stk]>rpsdata.rps1d.values[j]:
                                j = j - 1
                                continue
                            acctbalance=acctbalance-qty*price
                            stkpos[stk]=qty
                            entrypri[stk]=price
                            maxpri[stk]=price
                            stkbse[stk]=0
                            change=True
                            j = j - 1
                        except:
                            j=j-1
                    gap=datetime.datetime.now()-timepot
                    #print gap
            except:
                print 'what happened'
            keys = list(stkpos.keys())
            if change:
                change=False
                fileout.write(tdate[i] + ':\t')
                for stk in keys:
                    fileout.write(stk+'\t'+str(stkpos[stk])+'\t')
                fileout.write('\n')
            BSE=BSE+1
            acct[tdate[i]]=acctbalance+count(stkpos,tdate[i])
            print tdate[i],acct[tdate[i]]
        except:
            print 'error'
    return acct

def weaktest(tothold):
    acct={}
    cash={}
    BSE=5
    acctbalance=100000000.0
    stkpos={}
    entrypri={}
    expired={}
    maxpri={}
    change=False
    for i in range(0,len(tdate)):
        if tdate[i]=='2015-10-08':
            print
        try:
            if BSE>=5:
                fileout.write('held expired\n')
                keys=list(stkpos.keys())
                for stk in keys:
                    try:
                        price=loadpoint(stk,tdate[i],'openP')
                        if loadpoint(stk,tdate[i],'total')<0.5*stkpos[stk]:      #
                            raise Exception("no enough volumn")
                        acctbalance=acctbalance+float(price)*stkpos[stk]
                        change = True
                        del stkpos[stk]
                        del entrypri[stk]
                    except:
                        expired[stk]=stkpos[stk]
                        print 'can not sell ',stk
                sumstk=0
                BSE = 0
            keys = list(stkpos.keys())
            for stk in keys:
                try:
                    price = loadpoint(stk, tdate[i], 'openP')
                    if stopwin(stk,tdate[i-1],entrypri[stk]) or expired.has_key(stk):#or shdata.close[tdate[i-1]]<shdata.MA60[tdate[i-1]]:
                        if loadpoint(stk, tdate[i], 'total') < 0.5*stkpos[stk]: #
                            raise Exception("no enough volumn")
                        acctbalance = acctbalance + float(price) * stkpos[stk]
                        change = True
                        del stkpos[stk]
                        del entrypri[stk]
                        if expired.has_key(stk):
                            del expired[stk]
                except:
                    pass
            try:
                print datetime.datetime.now().microsecond
                if len(stkpos)<tothold :#and shdata.close[tdate[i-1]]>shdata.MA60[tdate[i-1]]:
                    rpsdata = loadrpsblocks(tdate[i-1])#loadrpsbydate(tdate[i-1])
                    rpsdata.sort(['rps65d'], ascending=False, inplace=True)
                    percash = acctbalance / (tothold-len(stkpos))
                  #  rpsblock=loadrpsblocks(tdate[i-1])
                j = 2500
                print datetime.datetime.now().microsecond
                while len(stkpos)<tothold and j>2000:#and shdata.close[tdate[i-1]]>shdata.MA60[tdate[i-1]] and j<500:
                    try:
                        stk=rpsdata.symbol.values[j]
                        if stkpos.has_key(stk) :#or rpsblock.rps1d[stk]>rpsdata.rps1d.values[j]:
                            j=j-1
                            continue
                        price = float(loadpoint(stk, tdate[i], 'openP'))
                        qty = round(percash / price / 100) * 100
                        if loadpoint(stk, tdate[i], 'total') < 0.5 * qty or np.isnan(rpsdata.rps260d.values[j]):  #
                            raise Exception("no enough volumn")
                        acctbalance=acctbalance-qty*price
                        stkpos[stk]=qty
                        entrypri[stk]=price
                        change=True
                        j = j - 1
                    except:
                        j=j-1
                print datetime.datetime.now().microsecond
            except:
                pass
            keys = list(stkpos.keys())
            if change:
                change=False
                fileout.write(tdate[i] + ':\t')
                for stk in keys:
                    fileout.write(stk+'\t'+str(stkpos[stk])+'\t')
                fileout.write('\n')
            BSE=BSE+1
            acct[tdate[i]]=acctbalance+count(stkpos,tdate[i])
            print tdate[i],acct[tdate[i]]
        except:
            print 'error'
    return acct
acct=strongtest(100)
maxcash=0
#drawline('2012-08-03','2012-12-10',acct)
position={}
startnet=float(shdata.close[np.where(tdate==Sdate)[0]])
for i in range(np.where(tdate==Sdate)[0],np.where(tdate==Edate)[0]+1):  
    position.setdefault('date', []).append(tdate[i])
    position.setdefault('net',[]).append(acct[tdate[i]]/100000000)
    position.setdefault('hs300', []).append(float(shdata['close'][tdate[i]]) /startnet )
#    position.setdefault('gap',[]).append(acct[tdate[i]]/100000000-float(shdata['close'][tdate[i]]) / float(shdata.close.values[60]))
    filetralist.write(tdate[i]+'\t'+str(acct[tdate[i]]/100000000)+'\t'+str(float(shdata['close'][tdate[i]])/startnet) +'\n')

df = pd.DataFrame(position)
df.index = df['date']
df = df.sort_index()
df.plot()
plt.show()
fileout.close()
filetralist.close()