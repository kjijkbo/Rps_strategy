
import tushare as ts
import pandas as pd
import MySQLdb as mysql
import talib as ta
import pandas as pd
import sqlite3
import datetime
import matplotlib.pyplot as plt
import numpy as np
import traceback
from sqlalchemy.sql.expression import false

conn=sqlite3.connect('E:\\sqliteDb\\securityevaluationdb.db')
def datelist(start,end):
    if start[5:7]<'06':
        temp=start[:4]+'-06-01' 
    else:
        temp=str(int(start[:4]) + 1) + '-06-01'
    if start[:4]==end[:4] or end<temp:
        sh = ts.get_k_data('399300', index=True, start=start, end=end)
        sh.sort(['date'], ascending=True, inplace=True)
        sh.index=sh['date']
        return sh.index.values, sh
    sh=ts.get_k_data('399300',index=True,start=start,end=temp)
    starttemp=temp
    temp=str(int(starttemp[:4])+1)+'-06-01'
    while end>temp:
        sh1=ts.get_k_data('399300',index=True,start=starttemp,end=temp)
        sh=pd.merge(sh,sh1,how='outer')
        starttemp = temp
        temp = str(int(starttemp[:4]) + 1) + '-06-01'
    sh1 = ts.get_k_data('399300', index=True, start=starttemp, end=end)
    sh = pd.merge(sh, sh1, how='outer')
    sh.sort(['date'], ascending=True, inplace=True)
    sh.index=sh['date']
    return sh.index.values,sh

tdate,shdata = datelist('2008-12-01', '2009-12-01')
sql=[]
count=0
for i in range(5,len(tdate)):
    print tdate[i]
    date=tdate[i]
    datestart = tdate[np.where(tdate==date)[0]-2][0]
    sql1='select stockstk.stockID,dateTime,avg(rps1d) as rps1d,avg(rps3d) as rps3d,avg(rps5d) as rps5d,avg(rps10d) as rps10d,avg(rps20d) as rps20d,avg(rps30d) as rps30d,avg(rps65d) as rps65d,avg(rps130d) as rps130d,avg(rps260d) as rps260d from stockrpshist,stockstk where stockstk.stockid=stockrpshist.stockid and date(dateTime) between \''+datestart+'\' and \'' + date + '\' group by stockstk.stockCode'
    df=pd.read_sql(sql1, conn, index_col=None, coerce_float=True, params=None, parse_dates=None, columns=None,chunksize=None)
    sql='begin;\n'
    for j in range(0,len(df)):
        sql+="insert into stockrpsavg(stockID,dateTime,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d) values(%d,'%s',%f,%f,%f,%f,%f,%f,%f,%f,%f)" % (df.stockID[j],df.dateTime[j],df.rps1d[j],df.rps3d[j],df.rps5d[j],df.rps10d[j],df.rps20d[j],df.rps30d[j],df.rps65d[j],df.rps130d[j],df.rps260d[j])  
        count+=1
        if count>=10000:
            sql+='commit;'
            results=conn.execute(sql)
            conn.commit()
            count=0
            sql='begin;\n'
        #conn.execute(sql)
        #conn.commit()  