
import tushare as ts
import pandas as pd
import MySQLdb as mysql
import talib as ta
import pandas as pd
import sqlite3
import datetime
import matplotlib.pyplot as plt
import numpy as np
import traceback
from sqlalchemy.sql.expression import false

#conn=sqlite3.connect('c:\\sqlite\\securityevaluationdb.db')
db_stk=mysql.connect('localhost','root','Aa888888','stockinfo')
cursor_stk=db_stk.cursor()
def datelist(start,end):
    if start[5:7]<'06':
        temp=start[:4]+'-06-01' 
    else:
        temp=str(int(start[:4]) + 1) + '-06-01'
    if start[:4]==end[:4] or end<temp:
        sh = ts.get_k_data('399300', index=True, start=start, end=end)
        sh.sort(['date'], ascending=True, inplace=True)
        sh.index=sh['date']
        return sh.index.values, sh
    sh=ts.get_k_data('399300',index=True,start=start,end=temp)
    starttemp=temp
    temp=str(int(starttemp[:4])+1)+'-06-01'
    while end>temp:
        sh1=ts.get_k_data('399300',index=True,start=starttemp,end=temp)
        sh=pd.merge(sh,sh1,how='outer')
        starttemp = temp
        temp = str(int(starttemp[:4]) + 1) + '-06-01'
    sh1 = ts.get_k_data('399300', index=True, start=starttemp, end=end)
    sh = pd.merge(sh, sh1, how='outer')
    sh.sort(['date'], ascending=True, inplace=True)
    sh.index=sh['date']
    return sh.index.values,sh

tdate,shdata = datelist('2009-11-24', '2016-11-11')
def loadrpsblocks(date):
    datestart = tdate[np.where(tdate==date)[0]-2][0]
    timepot=datetime.datetime.now()
    #sql='select stockstk.stockcode,stockstk.stockmarket,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d from stockrpshist,stockstk ' \
     #   'where stockstk.stockid=stockrpshist.stockid and dateTime<=\''+date+'\' order by datetime desc limit 5'
    sql='select stockstk.stockID,dateTime,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d from stockrpshist,stockstk ' \
        'where stockstk.stockid=stockrpshist.stockid and dateTime between \''+datestart+'\' and \'' + date + '\''
    #sql='select stockstk.stockcode,stockstk.stockmarket,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d from stockrpsavg,stockstk ' \
    #    'where stockstk.stockid=stockrpsavg.stockid and rps65d>85 and rps130d>85 and date(dateTime)=\''+date+'\''
    cursor_stk.execute(sql)
    results = cursor_stk.fetchall()
    print(len(results))
    data = {}
    data.setdefault('stockID',[])
    try:
        for row in results:
            if row[0] in data['stockID']:
                index=data['stockID'].index(row[0])
                data['rps1d'][index]=data['rps1d'][index]+row[2]
                data['rps3d'][index] = data['rps3d'][index] + row[3]
                data['rps5d'][index] = data['rps5d'][index] + row[4]
                data['rps10d'][index] = data['rps10d'][index] + row[5]
                data['rps20d'][index] = data['rps20d'][index] + row[6]
                data['rps30d'][index] = data['rps30d'][index] + row[7]
                data['rps65d'][index] = data['rps65d'][index] + row[8]
                data['rps130d'][index] = data['rps130d'][index] + row[9]
                data['rps260d'][index] = data['rps260d'][index] + row[10]
            else:
                data.setdefault('stockID', []).append(row[0])
                data.setdefault('dateTime', []).append(date)
                data.setdefault('rps1d', []).append(row[2])
                data.setdefault('rps3d', []).append(row[3])
                data.setdefault('rps5d', []).append(row[4])
                data.setdefault('rps10d', []).append(row[5])
                data.setdefault('rps20d', []).append(row[6])
                data.setdefault('rps30d', []).append(row[7])
                data.setdefault('rps65d', []).append(row[8])
                data.setdefault('rps130d', []).append(row[9])
                data.setdefault('rps260d', []).append(row[10])
    except:
        print
    
    for i in range(0,len(data['stockID'])):
        data['rps1d'][i]=data['rps1d'][i]/3
        data['rps3d'][i] = data['rps3d'][i] / 3
        data['rps5d'][i] = data['rps5d'][i] / 3
        data['rps10d'][i] = data['rps10d'][i] / 3
        data['rps20d'][i] = data['rps20d'][i] / 3
        data['rps30d'][i] = data['rps30d'][i] / 3
        data['rps65d'][i] = data['rps65d'][i] / 3
        data['rps130d'][i] = data['rps130d'][i] / 3
        data['rps260d'][i] /= 3
    
    df=pd.DataFrame(data)
    #df.index=df.stockID
    gap=datetime.datetime.now()-timepot
    print gap
    return df

sql=[]
count=0
for i in range(5,len(tdate)):
    sql1='SELECT * from stockrpshist WHERE dateTime=\''+tdate[i]+'\''
    sql2='SELECT * from stockrpsavg WHERE dateTime=\''+tdate[i]+'\''
    cursor_stk.execute(sql1)
    res1=cursor_stk.fetchall()
    cursor_stk.execute(sql2)
    res2=cursor_stk.fetchall()
    if len(res1)!=len(res2):
            print tdate[i]
pass
for i in range(5,len(tdate)):
    print tdate[i]
    date=tdate[i]
    datestart = tdate[np.where(tdate==date)[0]-2][0]
    #sql1='select stockstk.stockID,dateTime,avg(rps1d) as rps1d,avg(rps3d) as rps3d,avg(rps5d) as rps5d,avg(rps10d) as rps10d,avg(rps20d) as rps20d,avg(rps30d) as rps30d,avg(rps65d) as rps65d,avg(rps130d) as rps130d,avg(rps260d) as rps260d from stockrpshist,stockstk where stockstk.stockid=stockrpshist.stockid and date(dateTime) between \''+datestart+'\' and \'' + date + '\' group by stockstk.stockCode'
    #df=pd.read_sql(sql1, db_stk, index_col=None, coerce_float=True, params=None, parse_dates=None, columns=None,chunksize=None)
    df=loadrpsblocks(tdate[i])
    for j in range(0,len(df)):
        sql.append([int(df.stockID[j]),df.dateTime[j],df.rps1d[j],df.rps3d[j],df.rps5d[j],df.rps10d[j],df.rps20d[j],df.rps30d[j],df.rps65d[j],df.rps130d[j],df.rps260d[j]])  
        count+=1
        if count>=10000:
            cursor_stk.executemany("insert into stockrpsavg(stockID,dateTime,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",sql)
            db_stk.commit()
            count=0
            sql=[]
        #conn.execute(sql)
        #conn.commit()  
if count>0:
    cursor_stk.executemany("insert into stockrpsavg(stockID,dateTime,rps1d,rps3d,rps5d,rps10d,rps20d,rps30d,rps65d,rps130d,rps260d) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",sql)
    db_stk.commit()